﻿#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// Функція для перетворення числа типу double в рядок з двома знаками після коми
// value - значення типу double, яке потрібно перетворити в рядок
char* double_to_string(double value) {
    char* buffer = (char*)malloc(30 * sizeof(char)); // Виділення пам'яті для рядка
    snprintf(buffer, 30, "%.2f", value); // Перетворення числа в рядок з двома знаками після коми
    return buffer; // Повернення рядка
}

// Функція для обчислення значення f(n) = n
// n - вхідне значення
double func_n(int n) {
    return n; // Повернення значення n
}

// Функція для обчислення значення f(n) = log(n)
// n - вхідне значення
double func_log_n(int n) {
    if (n <= 0) // Перевірка, чи n не є від'ємним або нулем, оскільки логарифм таких чисел не визначений
        return -INFINITY; // Повернення негативної нескінченності для від'ємних або нульових значень
    return log(n); // Обчислення логарифму
}

// Функція для обчислення значення f(n) = n * log(n)
// n - вхідне значення
double func_n_log_n(int n) {
    if (n <= 0) // Перевірка, чи n не є від'ємним або нулем
        return 0; // Повернення нуля для від'ємних або нульових значень
    return n * log(n); // Обчислення n * log(n)
}

// Функція для обчислення значення f(n) = n^2
// n - вхідне значення
double func_n_squared(int n) {
    return n * n; // Обчислення квадрату числа n
}

// Функція для обчислення значення f(n) = 2^n
// n - вхідне значення
double func_2_n(int n) {
    return pow(2, n); // Обчислення 2 в степені n
}

// Функція для обчислення значення f(n) = n!
// Використовується цикл для обчислення факторіалу
// n - вхідне значення
double func_factorial(int n) {
    double result = 1; // Початкове значення факторіалу
    for (int i = 2; i <= n; ++i) // Цикл від 2 до n
        result *= i; // Обчислення факторіалу
    return result; // Повернення результату
}

// Функція для табулювання значень функцій від start до end
void tabulate_functions(int start, int end) {
    // Вивід заголовків стовпців з великими відступами
    printf("%5s%20s%20s%20s%20s%40s%50s\n", "n", "f(n)=n", "f(n)=log(n)", "f(n)=n*log(n)", "f(n)=n^2", "f(n)=2^n", "f(n)=n!");

    // Обчислення та вивід значень функцій для кожного n від start до end
    for (int i = start; i <= end; ++i) {
        // Перетворення чисел у рядки з двома знаками після коми
        char* n_str = double_to_string(func_n(i));
        char* log_n_str = double_to_string(func_log_n(i));
        char* n_log_n_str = double_to_string(func_n_log_n(i));
        char* n_squared_str = double_to_string(func_n_squared(i));
        char* two_n_str = double_to_string(func_2_n(i));
        char* factorial_str = double_to_string(func_factorial(i));

        // Вивід значень функцій у форматованому вигляді
        printf("%5d%20s%20s%20s%20s%40s%50s\n", i, n_str, log_n_str, n_log_n_str, n_squared_str, two_n_str, factorial_str);

        // Звільнення пам'яті, виділеної для рядків
        free(n_str);
        free(log_n_str);
        free(n_log_n_str);
        free(n_squared_str);
        free(two_n_str);
        free(factorial_str);
    }
}

int main() {
    int start = 0; // Початкове значення для табулювання
    int end = 50; // Кінцеве значення для табулювання
    tabulate_functions(start, end); // Виклик функції для табулювання функцій
    return 0; // Повернення 0 для позначення успішного завершення програми
}
