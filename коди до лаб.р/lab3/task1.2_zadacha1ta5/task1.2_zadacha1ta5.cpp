﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <windows.h>

// Функція для піднесення числа a у ступінь b
// a - основа, b - показник ступеня
double power(int a, int b) {
    double result = 1; // Початкове значення результату
    for (int i = 0; i < b; i++) {
        result *= a; // Піднесення до ступеня шляхом множення
    }
    return result; // Повернення результату
}

// Функція для порівняння двох чисел, використовується в qsort
int compare(const void* a, const void* b) {
    return (*(int*)b - *(int*)a); // Порівняння чисел для сортування у спадному порядку
}

// Функція для генерації випадкових чисел та повернення найбільшого можливого числа
// вісімкової системи числення
int max_octal_number(int size) {
    int* octal_digits = (int*)malloc(size * sizeof(int)); // Динамічне виділення пам'яті для масиву
    if (octal_digits == NULL) {
        printf("Помилка виділення пам'яті!\n");
        exit(1);
    }

    srand((unsigned int)time(0)); // Ініціалізація генератора випадкових чисел

    // Заповнення масиву випадковими цифрами від 0 до 7
    for (int i = 0; i < size; i++) {
        octal_digits[i] = rand() % 8;
    }

    // Сортування масиву у спадному порядку
    qsort(octal_digits, size, sizeof(int), compare);

    // Обчислення найбільшого можливого числа
    int max_number = 0;
    for (int i = 0; i < size; i++) {
        max_number = max_number * 8 + octal_digits[i];
    }

    free(octal_digits); // Звільнення пам'яті
    return max_number; // Повернення найбільшого можливого числа
}

int main() {
    // Налаштування кодування для української мови
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    // Завдання 1: Введення чисел користувачем
    int a, b;
    printf(">Завдання 1:\n");
    printf("Введіть значення a (0 <= a <= 10): ");
    scanf("%d", &a);
    if (a < 0 || a > 10) {
        printf("Помилка: значення a повинно бути в діапазоні від 0 до 10.\n");
        return 1;
    }

    printf("Введіть значення b (0 <= b < 20): ");
    scanf("%d", &b);
    if (b < 0 || b >= 20) {
        printf("Помилка: значення b повинно бути в діапазоні від 0 до 19.\n");
        return 1;
    }

    printf("Відповідь: a^b = %d^%d = %.0f\n", a, b, power(a, b)); // Виклик функції піднесення до ступеня та виведення результату

    // Завдання 5
    int size;
    printf("\n>Завдання 5:\n");
    printf("Введіть розмір масиву m (1 <= m <= 20): ");
    scanf("%d", &size);
    if (size < 1 || size > 20) {
        printf("Помилка: розмір масиву m повинен бути в діапазоні від 1 до 20.\n");
        return 1;
    }

    int max_number = max_octal_number(size); // Виклик функції для отримання найбільшого числа
    printf("Відповідь: Найбільше можливе число з цифр вісімкової системи (розмір масиву %d): %d\n", size, max_number); // Виведення результату

    return 0; // Повернення 0 для позначення успішного завершення програми
}
