﻿#include <stdio.h>
#include <stdlib.h>
#include <Windows.h>

#define MAX 100

// Структура вузла для черги
typedef struct QueueNode {
    int data; // Дані вузла
    struct QueueNode* next; // Вказівник на наступний вузол
} QueueNode;

// Структура черги
typedef struct Queue {
    QueueNode* front; // Вказівник на початок черги
    QueueNode* rear; // Вказівник на кінець черги
} Queue;

// Ініціалізація черги
Queue* createQueue() {
    Queue* q = (Queue*)malloc(sizeof(Queue)); // Виділення пам'яті для структури черги
    q->front = q->rear = NULL; // Ініціалізація вказівників
    return q;
}

// Перевірка, чи порожня черга
int isEmpty(Queue* q) {
    return q->front == NULL; // Черга порожня, якщо вказівник front є NULL
}

// Додавання елемента в чергу
void enqueue(Queue* q, int value) {
    QueueNode* temp = (QueueNode*)malloc(sizeof(QueueNode)); // Виділення пам'яті для нового вузла
    temp->data = value; // Записуємо дані у вузол
    temp->next = NULL; // Встановлюємо наступний вузол як NULL
    if (q->rear == NULL) { // Якщо черга порожня
        q->front = q->rear = temp; // Початок і кінець черги вказують на новий вузол
        return;
    }
    q->rear->next = temp; // Додаємо новий вузол в кінець черги
    q->rear = temp; // Зміщуємо rear до нового вузла
}

// Видалення елемента з черги
int dequeue(Queue* q) {
    if (isEmpty(q)) { // Якщо черга порожня
        return -1; // Повертаємо -1 як ознаку помилки
    }
    QueueNode* temp = q->front; // Зберігаємо вказівник на вузол для видалення
    q->front = q->front->next; // Зміщуємо початок черги до наступного вузла
    if (q->front == NULL) { // Якщо черга стала порожньою після видалення вузла
        q->rear = NULL; // Оновлюємо rear
    }
    int data = temp->data; // Зберігаємо дані вузла
    free(temp); // Звільняємо пам'ять
    return data; // Повертаємо дані вузла
}

// Відвідування вузла (в даному випадку просто друкуємо)
void visit(int node) {
    printf("%d ", node); // Виводимо вузол
}

// Рекурсивна функція для DFS
void DFSUtil(int graph[MAX][MAX], int node, int visited[MAX], int n) {
    visited[node] = 1; // Відзначаємо вузол як відвіданий
    visit(node); // Відвідуємо вузол (виводимо його)
    for (int i = 0; i < n; i++) { // Проходимо по всіх вузлах
        if (graph[node][i] == 1 && !visited[i]) { // Якщо існує ребро і вузол не відвіданий
            DFSUtil(graph, i, visited, n); // Рекурсивно викликаємо DFS для наступного вузла
        }
    }
}

// Функція для запуску DFS
void DFS(int graph[MAX][MAX], int start, int n) {
    int visited[MAX] = { 0 }; // Ініціалізація масиву відвіданих вузлів
    printf("DFS починаючи з вузла %d:\n", start);
    DFSUtil(graph, start, visited, n); // Виклик рекурсивної функції DFS
    printf("\n");
}
// Функція для BFS
void BFS(int graph[MAX][MAX], int start, int n) {
    int visited[MAX] = { 0 }; // Ініціалізація масиву відвіданих вузлів
    Queue* q = createQueue(); // Створення черги
    enqueue(q, start); // Додавання початкового вузла в чергу
    visited[start] = 1; // Відзначаємо вузол як відвіданий
    printf("BFS починаючи з вузла %d:\n", start);
    while (!isEmpty(q)) { // Поки черга не порожня
        int node = dequeue(q); // Видаляємо вузол з черги
        visit(node); // Відвідуємо вузол (виводимо його)
        for (int i = 0; i < n; i++) { // Проходимо по всіх вузлах
            if (graph[node][i] == 1 && !visited[i]) { // Якщо існує ребро і вузол не відвіданий
                enqueue(q, i); // Додаємо вузол до черги
                visited[i] = 1; // Відзначаємо вузол як відвіданий
            }
        }
    }
    printf("\n");
}
int main() {
    // Налаштування кодування для української мови
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    int n = 19; // Кількість вузлів
    int graph[MAX][MAX] = { 0 }; // Ініціалізація графа

    // Встановлення ребер графа відповідно до наданого маршруту
    graph[0][1] = graph[1][0] = 1; // Київ - Житомир
    graph[1][2] = graph[2][1] = 1; // Житомир - Новоград-Волинський
    graph[2][3] = graph[3][2] = 1; // Новоград-Волинський - Рівне
    graph[3][4] = graph[4][3] = 1; // Рівне - Луцьк

    graph[1][5] = graph[5][1] = 1; // Житомир - Бердичів
    graph[5][6] = graph[6][5] = 1; // Бердичів - Вінниця
    graph[6][7] = graph[7][6] = 1; // Вінниця - Хмельницький
    graph[7][8] = graph[8][7] = 1; // Хмельницький - Тернопіль

    graph[1][9] = graph[9][1] = 1; // Житомир - Шепетівка

    graph[0][10] = graph[10][0] = 1; // Київ - Біла Церква
    graph[10][11] = graph[11][10] = 1; // Біла Церква - Умань

    graph[10][12] = graph[12][10] = 1; // Біла Церква - Черкаси
    graph[12][13] = graph[13][12] = 1; // Черкаси - Кременчук

    graph[10][14] = graph[14][10] = 1; // Біла Церква - Полтава
    graph[14][15] = graph[15][14] = 1; // Полтава - Харків

    graph[0][16] = graph[16][0] = 1; // Київ - Прилуки
    graph[16][17] = graph[17][16] = 1; // Прилуки - Суми
    graph[16][18] = graph[18][16] = 1; // Прилуки - Миргород

    // Виконання DFS
    DFS(graph, 0, n);

    // Виконання BFS
    BFS(graph, 0, n);

    return 0;
}
