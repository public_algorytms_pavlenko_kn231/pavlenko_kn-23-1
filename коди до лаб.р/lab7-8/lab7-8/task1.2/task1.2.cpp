﻿#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <windows.h>

#define MAX_NODES 19 // Кількість вузлів у графі

// Матриця суміжності для представлення графа
int adjacency[MAX_NODES][MAX_NODES] = {
    {0, 135, 0, 0, 0, 135, 0, 0, 0, 0, 78, 0, 0, 0, 0, 0, 128, 0, 0},
    {181, 0, 80, 0, 0, 38, 0, 0, 0, 115, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 80, 0, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 100, 0, 68, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 68, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {135, 38, 0, 0, 0, 0, 73, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 73, 0, 110, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 110, 0, 104, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 104, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 115, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {78, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 115, 146, 0, 181, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 115, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 146, 0, 0, 105, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 105, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 181, 0, 0, 0, 0, 130, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 130, 0, 0, 0, 0},
    {128, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 175, 109},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 175, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 109, 0, 0}
};

int path_lengths[MAX_NODES]; // Массив для зберігання довжин шляхів
bool visited_nodes[MAX_NODES]; // Массив для зберігання інформації про відвідані вузли

// Функція для реалізації DFS (Пошук в Глибину)
void DepthFirstSearch(int current_node, int current_distance) {
    visited_nodes[current_node] = true; // Відзначаємо вузол як відвіданий
    path_lengths[current_node] = current_distance; // Записуємо відстань до вузла
    printf("Місто %d, Відстань: %d\n", current_node, current_distance); // Виводимо інформацію про вузол

    for (int i = 0; i < MAX_NODES; i++) { // Проходимо по всіх сусідніх вузлах
        if (adjacency[current_node][i] && !visited_nodes[i]) { // Якщо існує ребро і вузол не відвіданий
            DepthFirstSearch(i, current_distance + adjacency[current_node][i]); // Рекурсивно викликаємо DFS для наступного вузла
        }
    }
}

// Функція для реалізації BFS (Пошук в Ширину)
void BreadthFirstSearch(int start_node) {
    bool* explored = (bool*)malloc(MAX_NODES * sizeof(bool)); // Виділяємо пам'ять для масиву відвіданих вузлів
    for (int i = 0; i < MAX_NODES; i++) // Ініціалізуємо масив
        explored[i] = false;

    path_lengths[start_node] = 0; // Відстань до стартового вузла дорівнює 0
    explored[start_node] = true; // Відзначаємо стартовий вузол як відвіданий
    printf("Місто %d, Відстань: %d\n", start_node, path_lengths[start_node]); // Виводимо інформацію про стартовий вузол

    int queue[MAX_NODES]; // Масив для черги
    int front = 0, rear = 0; // Ініціалізуємо індекси черги
    queue[rear++] = start_node; // Додаємо стартовий вузол до черги

    while (front != rear) { // Поки черга не порожня
        int current_node = queue[front++]; // Видаляємо вузол з черги
        for (int i = 0; i < MAX_NODES; i++) { // Проходимо по всіх сусідніх вузлах
            if (adjacency[current_node][i] && !explored[i]) { // Якщо існує ребро і вузол не відвіданий
                queue[rear++] = i; // Додаємо вузол до черги
                explored[i] = true; // Відзначаємо вузол як відвіданий
                path_lengths[i] = path_lengths[current_node] + adjacency[current_node][i]; // Оновлюємо відстань до вузла
                printf("Місто %d, Відстань: %d\n", i, path_lengths[i]); // Виводимо інформацію про вузол
            }
        }
    }
    free(explored); // Звільняємо пам'ять
}

// Головна функція програми
int main() {
    SetConsoleCP(1251); // Встановлюємо кодування для введення в консоль
    SetConsoleOutputCP(1251); // Встановлюємо кодування для виведення в консоль

    int start_vertex = 0; // Початковий вузол (Київ)
    printf("Відстань від Києва до інших міст:\n");

    // Виконання DFS
    printf("DFS:\n");
    DepthFirstSearch(start_vertex, 0); // Викликаємо функцію DFS від початкового вузла
    printf("\n");

    // Виконання BFS
    printf("BFS:\n");
    BreadthFirstSearch(start_vertex); // Викликаємо функцію BFS від початкового вузла
    printf("\n");

    return 0; // Завершуємо програму
}

