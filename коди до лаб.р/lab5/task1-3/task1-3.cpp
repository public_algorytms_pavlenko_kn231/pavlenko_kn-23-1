﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#include <windows.h>
#include <time.h>

#define MAX 100 // Максимальний розмір стеку для обчислення виразу
#define ITERATIONS 10 // Кількість повторень для точного вимірювання

// Структура вузла двозв'язного списку
typedef struct Node {
    int data;
    struct Node* next;
    struct Node* prev;
} Node;

// Функція створення нового вузла
Node* createNode(int data) {
    Node* newNode = (Node*)malloc(sizeof(Node));
    newNode->data = data;
    newNode->next = NULL;
    newNode->prev = NULL;
    return newNode;
}

// Функція додавання елемента в кінець списку
void append(Node** head, int data) {
    Node* newNode = createNode(data);
    if (*head == NULL) {
        *head = newNode;
        return;
    }
    Node* temp = *head;
    while (temp->next != NULL) {
        temp = temp->next;
    }
    temp->next = newNode;
    newNode->prev = temp;
}

// Функція сортування вибором для двозв'язного списку
void selectionSortDLL(Node* head) {
    Node* i, * j, * min;
    for (i = head; i->next != NULL; i = i->next) {
        min = i;
        for (j = i->next; j != NULL; j = j->next) {
            if (j->data < min->data) {
                min = j;
            }
        }
        if (min != i) {
            int temp = i->data;
            i->data = min->data;
            min->data = temp;
        }
    }
}

// Функція сортування вставками для масиву
void insertionSortArray(int arr[], int n) {
    int i, key, j;
    for (i = 1; i < n; i++) {
        key = arr[i];
        j = i - 1;
        while (j >= 0 && arr[j] > key) {
            arr[j + 1] = arr[j];
            j = j - 1;
        }
        arr[j + 1] = key;
    }
}

// Функція сортування вставками для двозв'язного списку
void insertionSortDLL(Node* head) {
    if (head == NULL || head->next == NULL) return;
    Node* sorted = NULL;
    Node* current = head;
    while (current != NULL) {
        Node* next = current->next;
        if (sorted == NULL || sorted->data >= current->data) {
            current->next = sorted;
            if (sorted != NULL) sorted->prev = current;
            sorted = current;
            sorted->prev = NULL;
        }
        else {
            Node* temp = sorted;
            while (temp->next != NULL && temp->next->data < current->data) {
                temp = temp->next;
            }
            current->next = temp->next;
            if (temp->next != NULL) temp->next->prev = current;
            temp->next = current;
            current->prev = temp;
        }
        current = next;
    }
}

// Функція для виведення масиву
void printArray(int arr[], int size) {
    int i;
    for (i = 0; i < size; i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

// Функція для виведення двозв'язного списку
void display(Node* node) {
    Node* last = NULL;
    printf("Перегляд у прямому напрямку:\n");
    while (node != NULL) {
        printf("%d ", node->data);
        last = node;
        node = node->next;
    }
    if (last == NULL) {
        printf("\nСписок порожній.\n");
        return;
    }
    printf("\nПерегляд у зворотному напрямку:\n");
    while (last != NULL) {
        printf("%d ", last->data);
        last = last->prev;
    }
    printf("\n");
}

// Функція для знищення двозв'язного списку
void destroyList(Node** head) {
    Node* current = *head;
    Node* next;
    while (current != NULL) {
        next = current->next;
        free(current);
        current = next;
    }
    *head = NULL;
}

// Функція для вимірювання часу сортування масиву
double measureArraySort(int arr[], int n, void (*sortFunc)(int[], int)) {
    double total_time = 0;
    for (int i = 0; i < ITERATIONS; i++) {
        int* tempArr = (int*)malloc(n * sizeof(int));
        memcpy(tempArr, arr, n * sizeof(int));
        clock_t start = clock();
        sortFunc(tempArr, n);
        clock_t end = clock();
        total_time += ((double)(end - start)) / CLOCKS_PER_SEC;
        free(tempArr);
    }
    return total_time / ITERATIONS;
}

// Функція для вимірювання часу сортування двозв'язного списку
double measureListSort(Node* head, void (*sortFunc)(Node*)) {
    double total_time = 0;
    for (int i = 0; i < ITERATIONS; i++) {
        Node* tempHead = NULL;
        Node* temp = head;
        while (temp != NULL) {
            append(&tempHead, temp->data);
            temp = temp->next;
        }
        clock_t start = clock();
        sortFunc(tempHead);
        clock_t end = clock();
        total_time += ((double)(end - start)) / CLOCKS_PER_SEC;
        destroyList(&tempHead);
    }
    return total_time / ITERATIONS;
}

int main() {
    // Налаштування кодування для української мови
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    int choice, data, n, i;
    int* arr;
    Node* head = NULL;
    clock_t start, end;
    double time_taken_sec;
    double time_taken_ms;
    double time_taken_us;
    double time_taken_ns;
    double time_taken_ps;

    double selection_sort_time = 0.0;
    double insertion_sort_array_time = 0.0;
    double insertion_sort_list_time = 0.0;
    double min_time = 0.0;

    do {
        printf("\nМеню:\n");
        printf("1. Завдання 1: Сортування вибором (двозв'язний список)\n");
        printf("2. Завдання 2: Сортування вставками (масив)\n");
        printf("3. Завдання 3: Сортування вставками (двозв'язний список)\n");
        printf("4. Порівняти сортування\n");
        printf("0. Вихід\n");
        printf("Введіть свій вибір: ");
        scanf("%d", &choice);

        switch (choice) {
        case 1:
            // Завдання 1: Сортування вибором (двозв'язний список)
            head = NULL;
            printf("Введіть кількість елементів для сортування: ");
            scanf("%d", &n);
            for (i = 0; i < n; i++) {
                data = rand() % 100;
                append(&head, data);
            }
            printf("Невідсортований список:\n");
            display(head);
            start = clock();
            selectionSortDLL(head);
            end = clock();
            printf("Відсортований список:\n");
            display(head);
            time_taken_sec = ((double)(end - start)) / CLOCKS_PER_SEC;
            time_taken_ms = time_taken_sec * 1000;
            time_taken_us = time_taken_sec * 1000000;
            time_taken_ns = time_taken_sec * 1000000000;
            time_taken_ps = time_taken_sec * 1000000000000;
            printf("Час сортування: %.6f секунд (%.3f мілісекунд) (%.3f мікросекунд) (%.3f наносекунд) (%.3f пікосекунд)\n",
                time_taken_sec, time_taken_ms, time_taken_us, time_taken_ns, time_taken_ps);
            destroyList(&head);
            break;

        case 2:
            // Завдання 2: Сортування вставками (масив)
            printf("Введіть кількість елементів для сортування: ");
            scanf("%d", &n);
            arr = (int*)malloc(n * sizeof(int));
            for (i = 0; i < n; i++) {
                arr[i] = rand() % 100;
            }
            printf("Невідсортований масив:\n");
            printArray(arr, n);
            start = clock();
            insertionSortArray(arr, n);
            end = clock();
            printf("Відсортований масив:\n");
            printArray(arr, n);
            time_taken_sec = ((double)(end - start)) / CLOCKS_PER_SEC;
            time_taken_ms = time_taken_sec * 1000;
            time_taken_us = time_taken_sec * 1000000;
            time_taken_ns = time_taken_sec * 1000000000;
            time_taken_ps = time_taken_sec * 1000000000000;
            printf("Час сортування: %.6f секунд (%.3f мілісекунд) (%.3f мікросекунд) (%.3f наносекунд) (%.3f пікосекунд)\n",
                time_taken_sec, time_taken_ms, time_taken_us, time_taken_ns, time_taken_ps);
            free(arr);
            break;

        case 3:
            // Завдання 3: Сортування вставками (двозв'язний список)
            head = NULL;
            printf("Введіть кількість елементів для сортування: ");
            scanf("%d", &n);
            for (i = 0; i < n; i++) {
                data = rand() % 100;
                append(&head, data);
            }
            printf("Невідсортований список:\n");
            display(head);
            start = clock();
            insertionSortDLL(head);
            end = clock();
            printf("Відсортований список:\n");
            display(head);
            time_taken_sec = ((double)(end - start)) / CLOCKS_PER_SEC;
            time_taken_ms = time_taken_sec * 1000;
            time_taken_us = time_taken_sec * 1000000;
            time_taken_ns = time_taken_sec * 1000000000;
            time_taken_ps = time_taken_sec * 1000000000000;
            printf("Час сортування: %.6f секунд (%.3f мілісекунд) (%.3f мікросекунд) (%.3f наносекунд) (%.3ф пікосекунд)\n",
                time_taken_sec, time_taken_ms, time_taken_us, time_taken_ns, time_taken_ps);
            destroyList(&head);
            break;

        case 4:
            // Порівняння сортувань
            printf("Введіть кількість елементів для сортування: ");
            scanf("%d", &n);

            // Створення масиву та списку з однаковими даними
            arr = (int*)malloc(n * sizeof(int));
            for (i = 0; i < n; i++) {
                arr[i] = rand() % 100;
            }

            head = NULL;
            for (i = 0; i < n; i++) {
                append(&head, arr[i]);
            }

            // Вимірювання часу для кожного методу сортування
            selection_sort_time = measureListSort(head, selectionSortDLL);
            insertion_sort_array_time = measureArraySort(arr, n, insertionSortArray);
            insertion_sort_list_time = measureListSort(head, insertionSortDLL);

            printf("Середній час сортування (вибіркою): %.6f секунд\n", selection_sort_time);
            printf("Середній час сортування (вставками, масив): %.6f секунд\n", insertion_sort_array_time);
            printf("Середній час сортування (вставками, двозв'язний список): %.6f секунд\n", insertion_sort_list_time);

            // Визначення найшвидшого методу
            min_time = fmin(fmin(selection_sort_time, insertion_sort_array_time), insertion_sort_list_time);
            if (min_time == selection_sort_time) {
                printf("Найшвидший метод: сортування вибіркою (двозв'язний список)\n");
            }
            else if (min_time == insertion_sort_array_time) {
                printf("Найшвидший метод: сортування вставками (масив)\n");
            }
            else {
                printf("Найшвидший метод: сортування вставками (двозв'язний список)\n");
            }

            free(arr);
            destroyList(&head);
            break;

        case 0:
            printf("Вихід.\n");
            break;

        default:
            printf("Невірний вибір.\n");
            break;
        }
    } while (choice != 0);

    return 0;
}
