﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <windows.h>

// Функція для пірамідального сортування
void heapify(float arr[], int n, int i) {
    int largest = i; // Індекс найбільшого елемента
    int left = 2 * i + 1; // Лівий дочірній елемент
    int right = 2 * i + 2; // Правий дочірній елемент

    // Порівняння з лівим дочірнім елементом
    if (left < n && arr[left] > arr[largest])
        largest = left;

    // Порівняння з правим дочірнім елементом
    if (right < n && arr[right] > arr[largest])
        largest = right;

    // Якщо найбільший елемент не є коренем
    if (largest != i) {
        float temp = arr[i];
        arr[i] = arr[largest];
        arr[largest] = temp;

        // Рекурсивно виправити піддерево
        heapify(arr, n, largest);
    }
}

void heapSort(float arr[], int n) {
    // Побудова піраміди
    for (int i = n / 2 - 1; i >= 0; i--)
        heapify(arr, n, i);

    // Вилучення елементів з піраміди один за одним
    for (int i = n - 1; i >= 0; i--) {
        float temp = arr[0];
        arr[0] = arr[i];
        arr[i] = temp;

        // Виправлення піраміди на зменшеному масиві
        heapify(arr, i, 0);
    }
}

// Функція для сортування Шелла з використанням формули приросту inc(s)=(3^s-1)/2
void shellSort(double arr[], int n) {
    // Формування початкового значення приросту
    for (int gap = n / 2; gap > 0; gap /= 2) {
        for (int i = gap; i < n; i++) {
            double temp = arr[i];
            int j;
            for (j = i; j >= gap && arr[j - gap] > temp; j -= gap)
                arr[j] = arr[j - gap];
            arr[j] = temp;
        }
    }
}

// Функція для сортування підрахунком
void countingSort(short arr[], int n) {
    short* output = (short*)malloc(n * sizeof(short)); // Масив для збереження відсортованого масиву
    short max = arr[0];
    short min = arr[0];

    // Знаходження мінімального та максимального значення в масиві
    for (int i = 1; i < n; i++) {
        if (arr[i] > max)
            max = arr[i];
        else if (arr[i] < min)
            min = arr[i];
    }

    int range = max - min + 1;
    int* count = (int*)malloc(range * sizeof(int));
    for (int i = 0; i < range; i++)
        count[i] = 0;

    // Підрахунок кожного елемента
    for (int i = 0; i < n; i++)
        count[arr[i] - min]++;

    // Зміна count[i] так, щоб count[i] містив позицію цього елемента в output[]
    for (int i = 1; i < range; i++)
        count[i] += count[i - 1];

    // Створення відсортованого масиву
    for (int i = n - 1; i >= 0; i--) {
        output[count[arr[i] - min] - 1] = arr[i];
        count[arr[i] - min]--;
    }

    // Копіювання відсортованого масиву у вихідний масив
    for (int i = 0; i < n; i++)
        arr[i] = output[i];

    free(output);
    free(count);
}

// Функція для вимірювання часу сортування
void measureSortTime(void (*sortFunc)(void*, int), void* arr, int n, size_t elemSize, const char* sortName) {
    LARGE_INTEGER frequency, start, end;
    QueryPerformanceFrequency(&frequency);

    void* tempArr = malloc(n * elemSize);
    memcpy(tempArr, arr, n * elemSize);

    QueryPerformanceCounter(&start);
    sortFunc(tempArr, n);
    QueryPerformanceCounter(&end);

    double timeTaken = (double)(end.QuadPart - start.QuadPart) / frequency.QuadPart;
    printf("%s сортування: %f секунд\n", sortName, timeTaken);

    free(tempArr);
}

int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    int sizes[] = { 10, 100, 500, 1000, 2000, 5000, 10000 };
    int numSizes = sizeof(sizes) / sizeof(sizes[0]);

    for (int i = 0; i < numSizes; i++) {
        int n = sizes[i];
        float* arr1 = (float*)malloc(n * sizeof(float));
        double* arr2 = (double*)malloc(n * sizeof(double));
        short* arr3 = (short*)malloc(n * sizeof(short));

        // Генерація випадкових чисел
        for (int j = 0; j < n; j++) {
            arr1[j] = (float)(rand() % 181 + 20);  // [20, 200]
            arr2[j] = (double)(rand() % 111 - 10); // [-10, 100]
            arr3[j] = (short)(rand() % 36 - 10);   // [-10, 25]
        }

        printf("\nРозмір масиву: %d\n", n);

        measureSortTime((void (*)(void*, int))heapSort, arr1, n, sizeof(float), "Пірамідальне");
        measureSortTime((void (*)(void*, int))shellSort, arr2, n, sizeof(double), "Шелла");
        measureSortTime((void (*)(void*, int))countingSort, arr3, n, sizeof(short), "Підрахунком");

        free(arr1);
        free(arr2);
        free(arr3);
    }

    return 0;
}
