﻿#include <stdio.h>
#include <time.h>
#include <windows.h>
struct DateTimeNow {
	unsigned short year;
	unsigned char month;
	unsigned char day;
	unsigned char hour;
	unsigned char minute;
	unsigned char second;
};
int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	time_t t;
	struct tm tm_info;
	time(&t);
	if (localtime_s(&tm_info, &t) != 0) {
		printf("Помилка отримання місцевого часу\n");
		return 1;
	}
	struct DateTimeNow dateTimeNow;
	dateTimeNow.year = tm_info.tm_year + 1900;
	dateTimeNow.month = tm_info.tm_mon + 1;
	dateTimeNow.day = tm_info.tm_mday;
	dateTimeNow.hour = tm_info.tm_hour;
	dateTimeNow.minute = tm_info.tm_min;
	dateTimeNow.second = tm_info.tm_sec;
	printf("CompactDateTime Structure:\n");
	printf("\tРік: %d\n\tМісяць: %d\n\tДень: %d\n", dateTimeNow.year,
		dateTimeNow.month, dateTimeNow.day);
	printf("\t->Година: %d\n\t->Хвилина: %d\n\t->Секунда: %d\n", dateTimeNow.hour,
		dateTimeNow.minute, dateTimeNow.second);
	printf("[1] Розмір пам'яті структури CompactDateTime: %lu bytes\n",
		sizeof(dateTimeNow));
	printf("[2] Розмір пам'яті tm (time.h): %lu bytes\n", sizeof(struct tm));
	return 0;
}
