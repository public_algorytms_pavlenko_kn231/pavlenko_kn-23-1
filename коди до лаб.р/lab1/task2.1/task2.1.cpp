﻿#include <stdio.h>
#include <windows.h>
struct SignValue {
	short value;
	struct {
		unsigned short sign : 1;
		unsigned short magnitude : 15;
	};
};

int main() {
	struct SignValue newsign;
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	printf("->Введіть цілочисельне значення (signed short): ");
	scanf_s("%hd", &newsign.value);

	newsign.sign = (newsign.value < 0) ? 1 : 0;
	newsign.magnitude = (newsign.value < 0) ? -newsign.value : newsign.value;

	printf("->>Значення: %d,\n->>Знак: %s\n", newsign.magnitude, (newsign.sign == 0) ? "позитивний (+)" : "негативний (-)");
	return 0;
}
