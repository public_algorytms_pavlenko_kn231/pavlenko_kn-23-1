﻿#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <windows.h>

// Структура об'єднання для зберігання дійсного числа типу float
union FloatRepresentation {
    float value;           // зберігання дійсного числа
    unsigned int bits;     // зберігання числа у вигляді набору бітів
    unsigned char bytes[4]; // зберігання числа у вигляді масиву байтів
};

// Функція для виведення числа побитово
void printBits(unsigned int num) {
    for (int i = 31; i >= 0; i--) { // Цикл для проходження через кожен біт числа
        printf("%d", (num >> i) & 1); // Виводимо кожен біт числа
        if (i % 8 == 0) { // Додаємо пробіл після кожних 8 бітів для зручності
            printf(" ");
        }
    }
    printf("\n");
}
// Функція для виведення числа побайтово
void printBytes(unsigned char bytes[]) {
    for (int i = 0; i < 4; i++) { // Цикл для проходження через кожен байт числа
        printf("%02X ", bytes[i]); // Виводимо кожен байт у шістнадцятковому форматі
    }
    printf("\n");
}
// Функція для виведення знака, мантиси і експоненти числа
void printFloatComponents(union FloatRepresentation floatRep) {
    unsigned int sign = (floatRep.bits >> 31) & 1; // Виділяємо знак числа
    unsigned int exponent = (floatRep.bits >> 23) & 0xFF; // Виділяємо експоненту (8 бітів)
    unsigned int mantissa = floatRep.bits & 0x7FFFFF; // Виділяємо мантису (23 біти)

    printf("Знак: %d\n", sign);
    printf("Експонента: %u (десяткове), 0x%02X (шістнадцяткове)\n", exponent, exponent);
    printf("Мантиса: 0x%06X\n", mantissa);
}
int main() {
    // Установлюємо кодування консолі для української мови
    SetConsoleOutputCP(1251);
    SetConsoleCP(1251);

    union FloatRepresentation floatRep; // Оголошення об'єднання для зберігання числа

    // Введення дійсного числа типу float користувачем
    printf("Введіть дійсне число типу float: ");
    scanf_s("%f", &floatRep.value); // Використання безпечного вводу

    // Виведення числа побитово
    printf("Значення побитово: ");
    printBits(floatRep.bits);

    // Виведення числа побайтово
    printf("Значення побайтово: ");
    printBytes(floatRep.bytes);

    // Виведення знака, мантиси і експоненти числа
    printf("Компоненти числа:\n");
    printFloatComponents(floatRep);

    // Визначення обсягу пам'яті, яку займає змінна користувацького типу
    printf("Обсяг пам'яті, яку займає змінна користувацького типу: %zu байт\n", sizeof(floatRep));
    return 0;
}
