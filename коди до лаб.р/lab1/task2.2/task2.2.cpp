﻿#include <stdio.h>
#include <windows.h>
int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	short value;
	printf("->Введіть цілочисельне значення (signed short): ");
	scanf_s("%hd", &value);
	int sign = (value < 0) ? -1 : 1;
	int absValue = (sign < 0) ? -value : value;
	printf("->>Значення: %d,\n->>Знак(+/-): %s\n", absValue, (sign == 1) ? "позитивний(+)" : "негативний(-)");
	return 0;
}
