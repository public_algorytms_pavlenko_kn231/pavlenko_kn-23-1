﻿#include <stdio.h>
#include <windows.h>
void printBinary(unsigned char num) {
	for (int i = 7; i >= 0; i--) {
		printf("%d", (num >> i) & 1);
	}
}
int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	signed char a = 5, b = 127, c, d = -120, e = -34;

	// а) 5 + 127
	c = a + b;
	printf("a) 5 + 127 = ");
	printBinary(c);
	printf("\n");

	// б) 2 - 3
	c = 2 - 3;
	printf("б) 2 - 3 = ");
	printBinary(c);
	printf("\n");

	// в) -120 - 34
	c = d - e;
	printf("в) -120 - 34 = ");
	printBinary(c);
	printf("\n");

	// г) (unsigned char)(-5)
	unsigned char result_g = (unsigned char)(-5);
	printf("г) (unsigned char)(-5) = ");
	printBinary(result_g);
	printf("\n");

	// д) 56 & 38
	c = 56 & 38;
	printf("д) 56 & 38 = ");
	printBinary(c);
	printf("\n");

	// е) 56 | 38
	c = 56 | 38;
	printf("е) 56 | 38 = ");
	printBinary(c);
	printf("\n");
	return 0;
}
