﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#include <windows.h>

#define MAX 100 // Максимальний розмір стеку для обчислення виразу

// Структура вузла двозв'язного списку
typedef struct Node {
    int data; // Дані вузла
    struct Node* next; // Вказівник на наступний вузол
    struct Node* prev; // Вказівник на попередній вузол
} Node;

// Функція створення нового вузла
Node* createNode(int data) {
    Node* newNode = (Node*)malloc(sizeof(Node)); // Виділення пам'яті для нового вузла
    newNode->data = data; // Ініціалізація даних вузла
    newNode->next = NULL; // Ініціалізація вказівника на наступний вузол
    newNode->prev = NULL; // Ініціалізація вказівника на попередній вузол
    return newNode; // Повернення вказівника на новий вузол
}

// Функція додавання елемента в кінець списку
void append(Node** head, int data) {
    Node* newNode = createNode(data); // Створення нового вузла
    if (*head == NULL) { // Якщо список порожній
        *head = newNode; // Новий вузол стає головою списку
        return;
    }
    Node* temp = *head; // Тимчасовий вузол для ітерації до кінця списку
    while (temp->next != NULL) { // Ітерація до кінця списку
        temp = temp->next;
    }
    temp->next = newNode; // Додавання нового вузла в кінець списку
    newNode->prev = temp; // Встановлення вказівника на попередній вузол
}

// Функція видалення елемента зі списку
void deleteNode(Node** head, int key) {
    Node* temp = *head; // Тимчасовий вузол для пошуку вузла з ключем
    while (temp != NULL && temp->data != key) { // Пошук вузла з ключем
        temp = temp->next;
    }
    if (temp == NULL) return; // Якщо вузол не знайдено
    if (temp->prev != NULL) { // Якщо вузол не є головою списку
        temp->prev->next = temp->next; // Встановлення вказівника наступного вузла
    }
    else {
        *head = temp->next; // Встановлення нового головного вузла
    }
    if (temp->next != NULL) { // Якщо вузол не є кінцевим вузлом
        temp->next->prev = temp->prev; // Встановлення вказівника попереднього вузла
    }
    free(temp); // Звільнення пам'яті видаленого вузла
}

// Функція виведення списку
void display(Node* node) {
    Node* last = NULL; // Ініціалізація змінної last для уникнення попередження
    printf("Перегляд у прямому напрямку:\n");
    while (node != NULL) { // Ітерація по списку в прямому напрямку
        printf("%d ", node->data);
        last = node; // Збереження останнього відвіданого вузла
        node = node->next;
    }
    if (last == NULL) {
        printf("\nСписок порожній.\n");
        return;
    }
    printf("\nПерегляд у зворотному напрямку:\n");
    while (last != NULL) { // Ітерація по списку в зворотному напрямку
        printf("%d ", last->data);
        last = last->prev;
    }
    printf("\n");
}

// Функція знищення списку
void destroyList(Node** head) {
    Node* current = *head; // Поточний вузол для ітерації
    Node* next;
    while (current != NULL) { // Ітерація по всьому списку
        next = current->next; // Збереження вказівника на наступний вузол
        free(current); // Звільнення пам'яті поточного вузла
        current = next; // Перехід до наступного вузла
    }
    *head = NULL; // Встановлення головного вузла в NULL
}

// Функція для обчислення арифметичного виразу в зворотній польській нотації
double evaluateRPN(char* expression) {
    double stack[MAX]; // Стек для обчислення
    int top = -1; // Індекс вершини стеку
    char* token = strtok(expression, " "); // Розділення виразу на токени
    while (token != NULL) { // Ітерація по токенам
        if (isdigit(token[0]) || (token[0] == '-' && isdigit(token[1]))) { // Якщо токен є числом (враховуючи від'ємні числа)
            stack[++top] = atof(token); // Перетворення числа в тип double і додавання в стек
        }
        else { // Якщо токен є оператором
            if (token[0] == 's') { // Перевірка на sqrt (унарна операція)
                double a = stack[top--]; // Витягування операнда зі стеку
                stack[++top] = sqrt(a); // Обчислення квадратного кореня і збереження в стек
            }
            else {
                double b = stack[top--]; // Витягування другого операнда зі стеку
                double a = stack[top--]; // Витягування першого операнда зі стеку
                switch (token[0]) { // Виконання операції залежно від оператора
                case '+': stack[++top] = a + b; break;
                case '-': stack[++top] = a - b; break;
                case '*': stack[++top] = a * b; break;
                case '/': stack[++top] = a / b; break;
                case '^': stack[++top] = pow(a, b); break;
                }
            }
        }
        token = strtok(NULL, " "); // Перехід до наступного токена
    }
    return stack[top]; // Повернення результату обчислення
}

int main() {
    // Налаштування кодування для української мови
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    Node* head = NULL; // Ініціалізація голови списку
    int choice, data;
    char expression[MAX];

    do {
        printf("\nМеню:\n");
        printf("1. Завдання 1: Основні функції роботи з двозв'язним списком\n");
        printf("2. Завдання 2: Програма роботи з двозв'язним списком\n");
        printf("3. Завдання 3: Обчислення арифметичного виразу (ЗПЗ)\n");
        printf("0. Вихід\n");
        printf("Введіть свій вибір: ");
        scanf("%d", &choice);

        switch (choice) {
        case 1:
            // Завдання 1: Основні функції роботи з двозв'язним списком
            printf("Введіть кількість елементів для додавання (7-10): ");
            int num_elements;
            scanf("%d", &num_elements);
            for (int i = 0; i < num_elements; i++) {
                printf("Введіть значення для додавання: ");
                scanf("%d", &data);
                append(&head, data); // Додавання елемента в список
            }
            display(head); // Виведення списку
            break;

        case 2:
            // Завдання 2: Програма роботи з двозв'язним списком
            do {
                printf("\nМеню для роботи з двозв'язним списком:\n");
                printf("1. Створення списку\n");
                printf("2. Додавання елемента\n");
                printf("3. Видалення елемента\n");
                printf("4. Виведення списку\n");
                printf("5. Знищення списку\n");
                printf("0. Вихід до головного меню\n");
                printf("Введіть свій вибір: ");
                scanf("%d", &choice);

                switch (choice) {
                case 1:
                    head = NULL; // Створення нового порожнього списку
                    printf("Список створено.\n");
                    break;
                case 2:
                    printf("Введіть значення для додавання: ");
                    scanf("%d", &data);
                    append(&head, data); // Додавання елемента в список
                    printf("Елемент додано.\n");
                    break;
                case 3:
                    printf("Введіть значення для видалення: ");
                    scanf("%d", &data);
                    deleteNode(&head, data); // Видалення елемента зі списку
                    printf("Елемент видалено.\n");
                    break;
                case 4:
                    display(head); // Виведення списку
                    break;
                case 5:
                    destroyList(&head); // Знищення списку
                    printf("Список знищено.\n");
                    break;
                case 0:
                    printf("Вихід до головного меню.\n");
                    break;
                default:
                    printf("Невірний вибір.\n");
                    break;
                }
            } while (choice != 0);
            break;
        case 3:
            // Завдання 3: Обчислення арифметичного виразу (ЗПЗ)
            printf("Введіть арифметичний вираз у зворотній польській нотації: ");
            scanf(" %[^\n]", expression); // Зчитування рядка з пробілами
            printf("Результат: %.2f\n", evaluateRPN(expression)); // Обчислення та виведення результату
            break;

        case 0:
            printf("Вихід.\n");
            break;

        default:
            printf("Невірний вибір.\n");
            break;
        }
    } while (choice != 0);

    return 0;
}
