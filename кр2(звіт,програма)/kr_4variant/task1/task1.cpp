﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <Windows.h>
#define MAX 100 // Максимальна довжина стеку і рядків
char stack[MAX]; // Стек для зберігання операторів
int top = -1; // Індекс верхнього елемента стеку
// Функція для додавання елемента у стек
void push(char c) {
	if (top < (MAX - 1)) {
		stack[++top] = c;
	}
	else {
		printf("Стек переповнений\n");
	}
}
// Функція для вилучення елемента зі стеку
char pop() {
	if (top != -1) {
		return stack[top--];
	}
	else {
		// Зміна тут: повертаємо символ, який точно не є частиною виразу
		return '\0';
	}
}
// Функція для визначення пріоритету операторів
int priority(char c) {
	if (c == '*' || c == '/') {
		return 2; // Найвищий пріоритет для множення і ділення
	}
	else if (c == '+' || c == '-') {
		return 1; // Нижчий пріоритет для додавання і віднімання
	}
	else {
		// Додано обробку дужок для коректного визначення пріоритету
		return (c == '(') ? 0 : -1;
	}
}
// Головна функція для перетворення інфіксного виразу в постфіксний
void infixToPostfix(char* infix, char* postfix) {
	int i = 0, j = 0; // Індекси для інфіксного і постфіксного рядків
	char item, x;
	push('('); // Додаємо '(' на дно стеку
	strcat(infix, ")"); // Додаємо ')' до кінця вхідного рядка
	item = infix[i];
	while (item != '\0') { // Обробляємо кожен символ до кінця рядка
		if (item == '(') {
			push(item);
		}
		else if (isdigit(item) || isalpha(item)) {
			postfix[j++] = item; // Додаємо операнди прямо в вихідний рядок
		}
		else if (strchr("+-*/", item)) {
			x = pop();
			while (x != '\0' && priority(x) >= priority(item)) {
				postfix[j++] = x;
				x = pop();
			}
			if (x != '\0') push(x);
			push(item); // Додаємо новий оператор у стек
		}
		else if (item == ')') {
			x = pop();
				while (x != '\0' && x != '(') {
					postfix[j++] = x;
					x = pop();
				}
		}
		else {
			printf("Некоректний елемент: %c\n", item); // Виводимо некоректний символ
		}
		item = infix[++i]; // Переходимо до наступного символу
	}
	postfix[j] = '\0'; // Означаємо кінець рядка
}
int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	char infix[MAX], postfix[MAX];
	printf("Введіть інфіксний вираз: ");
	gets_s(infix, MAX);
	infixToPostfix(infix, postfix);
	printf("Постфіксний вираз: %s\n", postfix);
	return 0;
}